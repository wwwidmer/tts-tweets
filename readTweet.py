import json
from random import randrange
import re
import sys
import time

import pyttsx
from TwitterAPI import TwitterAPI


consumer_key = 'RJiRy5nzn5QJGjuPo6dl0Csrk'
consumer_secret = '2Sa5J4mTBxVB1LKux5pIaJk9iqzqIFDFtBJgDYmY5pC7LORilC'
access_token_key = '703285985166139392-yGnp9mw6cQUQGn3CxwptElnrIDiuDz8'
access_token_secret = 'vL4SIKkLZDW0KxbFtSDSl8MTMhQNDfFEi5onlGN3OAtIW'

affirmatives = ['yes', 'go', 'do it', 'y']

def main(keyword):
    pyttsx_engine = pyttsx.init()
    pyttsx_engine.setProperty('rate', 30)
    while(True):
        print('Looking for tweets...')
        tweets = getTweets(keyword)
        num_read = 0 

        while(num_read < len(tweets)/4):
            random_tweet = tweets[randrange(0, len(tweets))]
            print('Will say: ', random_tweet)
            check = raw_input('Yes?')
            if check.lower() in affirmatives:
                pyttsx_engine.say(random_tweet)
                pyttsx_engine.runAndWait()
                num_read+=1
                time.sleep(2)


def getTweets(keyword):
    tweets = getTweetsFromApi(keyword.lstrip('#'))
    tweets = [re.sub('RT', '', t['text']) for t in tweets]
    return tweets


def getTweetsFromApi(keyword):
    api = TwitterAPI(
        consumer_key, consumer_secret, access_token_key, access_token_secret
    )
    return api.request('search/tweets', {'q':'#{}'.format(keyword)})


if __name__ == '__main__':
    
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print('Give me something to hashtag please.')
